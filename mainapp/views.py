from django.shortcuts import render
from .forms import CreateJadwalForm, DeleteJadwalForm
from .models import JadwalKegiatan

# Create your views here.

def index(request):
    return render(request, 'index.html')

def extra(request):
    return render(request, 'index2.html')

def jadwal_form(request):
    response = {'create_jadwal_form': CreateJadwalForm, 'delete_jadwal_form': DeleteJadwalForm}
    return render(request, 'jadwal_form.html', response)

def save_jadwal(request):
    response = {'save_status':''}
    if(request.method == 'POST'):
        form = CreateJadwalForm(request.POST)
        if (form.is_valid()):
            tanggal = form.cleaned_data['tanggal']
            hari = tanggal.strftime('%A')
            jam = form.cleaned_data['jam']
            nama_kegiatan = form.cleaned_data['nama_kegiatan']
            tempat = form.cleaned_data['tempat']
            kategori = form.cleaned_data['kategori']
            

            jadwal = JadwalKegiatan(
                tanggal = tanggal,
                hari = hari,
                jam = jam,
                nama_kegiatan = nama_kegiatan,
                tempat = tempat,
                kategori = kategori
            )

            jadwal.save()

            response['save_status'] = 'Yey, jadwal berhasil ditambahkan! :D'
        else:
            response['save_status'] = 'Yah, jadwal gagal ditambahkan! :('
    else:
        response['save_status'] = 'Wah, ada masalah saat mau mengakses page ini!'

    return render(request, 'save_jadwal.html', response)

def delete_jadwal(request):
    response = {'delete_status':''}
    if(request.method == 'POST'):
        form = DeleteJadwalForm(request.POST)
        if (form.is_valid()):
            jadwal = form.cleaned_data['choice']
            jadwal.delete()

            response['delete_status'] = 'Yey, jadwal berhasil dihapus! :D'
        else:
            response['delete_status'] = 'Yah, jadwal gagal dihapus! :('
    else:
        response['delete_status'] = 'Wah, ada masalah saat mau mengakses page ini!'

    return render(request, 'delete_jadwal.html', response)

def jadwal_view(request):
    jadwals = JadwalKegiatan.objects.all().values()

    response = {'jadwals':jadwals}
    return render(request, 'jadwal_view.html', response)