from django.db import models
from datetime import date
# Create your models here.

class JadwalKegiatan(models.Model):
    tanggal = models.DateField()
    hari = models.CharField(max_length=50)
    jam = models.TimeField()
    nama_kegiatan = models.CharField(max_length=50)
    tempat = models.CharField(max_length=50)
    kategori = models.CharField(max_length=50)

    
