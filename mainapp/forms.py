from django import forms
from .models import JadwalKegiatan

class CreateJadwalForm(forms.Form):
    tanggal_attrs = {
        'type':'date',
        'class': 'form-control',
    }

    jam_attrs = {
        'type':'time',
        'class': 'form-control',
    }

    kegiatan_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Nama kegiatan...'
    }

    tempat_attrs = {
        'type':'text',
        'class': 'form-control',
        'placeholder':'Nama tempat...'
    }

    tanggal = forms.DateTimeField(label='Tanggal', required=True, localize=True, widget=forms.DateInput(attrs=tanggal_attrs))
    jam = forms.TimeField(label='Jam', required=True, localize=True, widget=forms.TimeInput(attrs=jam_attrs))
    nama_kegiatan = forms.CharField(label='Nama Kegiatan', max_length=50,  required=True, widget=forms.TextInput(attrs=kegiatan_attrs))
    tempat = forms.CharField(label='Nama Tempat', max_length=50, required=True, widget=forms.TextInput(attrs=tempat_attrs))
    

    kategori_attrs = {
        'class': 'custom-select'
    }

    choices = [
        ('Rekreasi', 'Rekreasi'),
        ('Tugas','Tugas'),
        ('Ujian','Ujian')
    ]
    
    kategori = forms.ChoiceField(label='Kategori', choices = choices, required=True, widget=forms.Select(attrs=kategori_attrs))

class CustomModelChoiceField(forms.ModelChoiceField):
    def label_from_instance(self, obj):
        return "{} ({}) : {}, {}".format(obj.nama_kegiatan, obj.kategori, obj.hari, obj.tanggal)

class DeleteJadwalForm(forms.Form):
    choice_attrs = {
        'class':'custom-select'
    }

    jadwals = JadwalKegiatan.objects.all()
    choice = CustomModelChoiceField(label="Kegiatan", required=True, widget=forms.Select(choice_attrs), queryset=jadwals, to_field_name="nama_kegiatan")

